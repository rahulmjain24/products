const container = document.querySelector('.container')
const loader = document.querySelector('.lds-ring')

const URL = 'https://fakestoreapi.com/products'

function createError(message) {
    const h1 = document.createElement('h1')
    
    h1.className = 'error'
    h1.textContent = message

    return h1
}

function createProduct(product) {
    const mainDiv = document.createElement('div')
    const img = document.createElement('img')
    const info = document.createElement('div')
    const rating = document.createElement('div')
    const rate = document.createElement('span')
    const rateImg = document.createElement('img')
    const count = document.createElement('span')
    const countImg = document.createElement('img')
    const titleInfo = document.createElement('div')
    const title = document.createElement('p')
    const category = document.createElement('p')
    const price = document.createElement('p')

    mainDiv.classList.add('product')

    img.classList.add('image')
    img.src = product.image
    img.alt = 'Product Image'

    info.classList.add('info')

    rating.classList.add('rating')

    rate.classList.add('rate')

    rateImg.classList.add('icon')
    rateImg.src = 'img/star.png'
    rateImg.alt = 'star'
    rate.appendChild(rateImg)
    rate.appendChild(document.createTextNode(product.rating.rate))
    
    count.classList.add('count')

    countImg.classList.add('icon')
    countImg.src = 'img/user.png'
    countImg.alt = 'user'
    count.appendChild(countImg)
    count.appendChild(document.createTextNode(product.rating.count))

    rating.appendChild(rate)
    rating.appendChild(count)

    titleInfo.classList.add('title-info')

    title.classList.add('title')
    title.appendChild(document.createTextNode(product.title))

    category.classList.add('category')
    category.appendChild(document.createTextNode(product.category))

    titleInfo.appendChild(title)
    titleInfo.appendChild(category)

    price.classList.add('price')
    price.appendChild(document.createTextNode(`$${product.price}`))

    info.appendChild(rating)
    info.appendChild(titleInfo)
    info.appendChild(price)

    mainDiv.appendChild(img)
    mainDiv.appendChild(info)

    return mainDiv
}

fetch(URL)
    .then((products) => {
        return products.json()
    })
    .then((productsData) => {
        container.removeChild(loader)
        if(productsData.length === 0) {
            container.appendChild(createError('No Products!!!'))
        } else {
            productsData.forEach((product) => {
                container.appendChild(createProduct(product))
            })
        }
    })
    .catch((err) => {
        container.removeChild(loader)
        container.appendChild(createError('Something went wrong!!!'))
        console.log(err)
    })
