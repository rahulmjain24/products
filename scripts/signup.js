const form = document.querySelector('.form')

const firstName = document.querySelector('#first-name')
const lastName = document.querySelector('#last-name')
const email = document.querySelector('#email')
const password = document.querySelector('#password')
const conPass= document.querySelector('#con-password')
const tos = document.querySelector('#tos')

const error = document.querySelector('.error')
const after = document.querySelector('.after')

function setError(err, input) {
    input.parentElement.nextElementSibling.textContent = err
    input.parentElement.style.borderBottom = '3px solid red'
}

function setSuccess(input) {
    input.parentElement.nextElementSibling.textContent = ''
    input.parentElement.style.borderBottom = ''
}

function checkInputs() {
    let isValid = true

    if(firstName.value === '') {
        isValid = false
        setError("First name can't be empty", firstName)
    } else {
        setSuccess(firstName)
    }

    if(lastName.value === '') {
        isValid = false
        setError("Last name can't be empty", lastName)
    } else {
        setSuccess(lastName)
    }

    if(email.value === '' ) {
        isValid = false
        setError("Email can't be empty", email)
    } else {
        if(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email.value.trim())) {
            setSuccess(email)
        } else {
            isValid = false
            setError("Email is not valid", email)
        }       
    }

    if(password.value === '') {
        isValid = false
        setError("Password can't be empty and needs to be 8 character long", password)
    } else {
        if(password.value.length < 8) {
            isValid = false
            setError("Password needs to be 8 character long", password)
        } else {
            setSuccess(password)
        }
    }

    if(conPass.value === '') {
        isValid = false
        setError("Confrim Passoword can't be empty", conPass)
    } else {
        if(password.value !== conPass.value) {
            isValid = false
            setError("The password does not match", conPass)
        } else {
            setSuccess(conPass)
        } 
    }

    if(!tos.checked) {
        isValid = false
        tos.nextElementSibling.nextElementSibling.textContent = 'TOS needs to be accepted'
    } else {
        tos.nextElementSibling.nextElementSibling.textContent = ''
    }

    if(isValid) {
        form.remove()
        const success = document.createElement('h1')
        const goBack = document.createElement('a')
        
        success.classList.add('heading')
        success.classList.add('success')

        goBack.setAttribute('href','/')
        goBack.classList.add('btn')

        success.appendChild(document.createTextNode('The form has been submitted successfully!'))
        goBack.appendChild(document.createTextNode('Go back to products'))

        after.appendChild(success)
        after.appendChild(goBack)
    }
}

form.addEventListener('submit', (formEvent) => {
    formEvent.preventDefault()

    checkInputs()
})

